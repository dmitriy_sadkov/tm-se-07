# TM-SE-07
```
Task tm-se-07 I-TECO
```
## TECHNOLOGIES

* Maven 4.0
* Java SE 1.8
* Junit 4.11
* Jcabi 1.1

## DEVELOPER
```
Sadkov Dmitriy
dmitriy_sadkov@mail.ru
```
## BUILDING FROM SOURCE
```
-mvn clean install
```
## SOFTWARE REQUIREMENTS
```
-jdk 1.8
```

## USING THE PROJECT MANAGER
```
From the command-line
Download the project manager and run it with:
java -jar C:\Users\user\tm-se-07\target\tm-se-07-1.0.jar
```