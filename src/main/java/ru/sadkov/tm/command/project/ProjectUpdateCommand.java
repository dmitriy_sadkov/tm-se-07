package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;

public final class ProjectUpdateCommand extends AbstractCommand {


    @Override
    public String command() {
        return "project-update";
    }

    @Override
    public String description() {
        return "Update project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        final Project project = serviceLocator.getProjectService().showByName(projectName, serviceLocator.getUserService().getCurrentUser());
        if (project == null) {
            System.out.println("[ACCESS DENIED]");
            return;
        }
        System.out.println("[ENTER NEW NAME]");
        final String newName = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().update(project, newName, description);
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
