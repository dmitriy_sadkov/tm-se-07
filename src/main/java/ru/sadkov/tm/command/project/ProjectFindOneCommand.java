package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;

public final class ProjectFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-show";
    }

    @Override
    public String description() {
        return "Show project";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = serviceLocator.getScanner().nextLine();
        Project project = serviceLocator.getProjectService().showByName(projectName, serviceLocator.getUserService().getCurrentUser());
        if (project == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        System.out.println(project);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
