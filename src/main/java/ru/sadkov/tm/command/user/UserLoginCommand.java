package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public final class UserLoginCommand extends AbstractCommand {
    @Override
    public String command() {
        return "login";
    }

    @Override
    public String description() {
        return "Authorize user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[AUTHORIZATION]");
        System.out.println("[ENTER LOGIN]");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserService().userAuthorize(login, password)) {
            System.out.println("[YOU ARE SUCCESSFULLY LOG IN]");
            return;
        }
        System.out.println("[SOMETHING WRONG! TRY AGAIN]");

    }

    @Override
    public boolean safe() {
        return true;
    }
}
