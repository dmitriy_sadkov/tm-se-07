package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;

public final class UserUpdateInformationCommand extends AbstractCommand {
    @Override
    public String command() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "Updating User profile";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATING PROFILE]");
        if (serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[NO USER! PLEASE LOGIN]");
            return;
        }
        System.out.println("[ENTER NEW LOGIN]");
        final String newUserName = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserService().updateProfile(newUserName)) {
            System.out.println("[SUCCESS]");
        } else {
            System.out.println("[SOMETHING WRONG! TRY AGAIN]");
        }
    }

    @Override
    public boolean safe() {
        return false;
    }
}
