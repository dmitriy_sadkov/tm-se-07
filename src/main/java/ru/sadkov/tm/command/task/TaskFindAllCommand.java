package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-all-task";
    }

    @Override
    public String description() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASKS:]");
        List<Task> taskList = serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser());
        if (taskList == null || taskList.isEmpty()) {
            System.out.println("[NO TASKS]");
            return;
        }
        ListShowUtil.showList(taskList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
