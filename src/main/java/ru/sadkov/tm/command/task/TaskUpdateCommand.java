package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-update";
    }

    @Override
    public String description() {
        return "Update task";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("[ENTER TASK NAME]");
        final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        final Task task = serviceLocator.getTaskService().findTaskByName(taskName, serviceLocator.getUserService().getCurrentUser().getId());
        if (task == null) return;
        System.out.println("[ENTER NEW NAME]");
        final String newName = serviceLocator.getScanner().nextLine();
        if (newName == null || newName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        serviceLocator.getTaskService().update(task, newName);
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
