package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public final class TaskCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER NAME:]");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("[ENTER PROJECT NAME]");
        final String projectName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty()) {
            System.out.println("[INCORRECT NAME]");
            return;
        }

        serviceLocator.getTaskService().saveTask(taskName, projectName, serviceLocator.getUserService().getCurrentUser());
    }

    @Override
    public boolean safe() {
        return false;
    }
}
