package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Task;

public final class TaskFindOneCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-show";
    }

    @Override
    public String description() {
        return "Show task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TASK NAME]");
        final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        Task task = serviceLocator.getTaskService().findTaskByName(taskName, serviceLocator.getUserService().getCurrentUser().getId());
        if (task == null) {
            System.out.println("[NO SUCH TASK]");
            return;
        }
        System.out.println(task);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
