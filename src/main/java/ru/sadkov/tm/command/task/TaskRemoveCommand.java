package ru.sadkov.tm.command.task;

import ru.sadkov.tm.command.AbstractCommand;

public final class TaskRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-removeByName";
    }

    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        System.out.println("[ENTER NAME:]");
        final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        serviceLocator.getTaskService().removeTask(taskName, serviceLocator.getUserService().getCurrentUser());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
