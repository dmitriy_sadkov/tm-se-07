package ru.sadkov.tm.entity;

public abstract class AbstractEntity {
    protected String id;

    public AbstractEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
