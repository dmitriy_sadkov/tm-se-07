package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;

import java.util.List;

public interface ITaskService {
    ITaskRepository getTaskRepository();

    void setTaskRepository(ITaskRepository taskRepository);

    Task findTaskByName(String taskName, String userId);

    void saveTask(String taskName, String projectName, User currentUser);

    void removeTask(String taskName, User currentUser);

    List<Task> findAll(User user);

    void clearTasks(User user);

    void removeTaskForProject(String projectName, User currentUser);

    void update(Task task, String taskName);

}
