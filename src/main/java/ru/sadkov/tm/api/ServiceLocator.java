package ru.sadkov.tm.api;

import ru.sadkov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    IUserService getUserService();

    void setUserService(IUserService userService);

    List<AbstractCommand> getCommandList();

    Scanner getScanner();

    IProjectService getProjectService();

    void setProjectService(IProjectService projectService);

    ITaskService getTaskService();

    void setTaskService(ITaskService taskService);
}
