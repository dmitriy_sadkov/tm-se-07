package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {
    User findOne(String userId);

    User findByLogin(String login);

    List<User> findAll();

    void removeAll();

    void update(String userId, String login);

    void removeByLogin(String login);
}
