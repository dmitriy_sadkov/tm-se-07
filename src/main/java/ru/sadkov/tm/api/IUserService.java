package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.User;


import java.security.NoSuchAlgorithmException;

public interface IUserService {
    User getCurrentUser();

    void setCurrentUser(User currentUser);

    void userRegister(User user) throws NoSuchAlgorithmException;

    boolean userAuthorize(String login, String password) throws NoSuchAlgorithmException;

    void userLogout();

    void updatePassword(String newPassword) throws NoSuchAlgorithmException;

    User showUser();

    boolean updateProfile(String newUserName);

    void addTestUsers() throws NoSuchAlgorithmException;

    boolean isAuth();
}
