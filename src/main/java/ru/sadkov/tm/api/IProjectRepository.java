package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;

import java.util.Collection;

public interface IProjectRepository extends IRepository<Project> {
    Collection<Project> findAll(String userId);

    void remove(String userId, String projectName);

    void removeAll(String userId);

    Project findOne(String projectId, String userId);

    boolean merge(Project project);

    void update(String userId, String id, String description, String projectName);

}
