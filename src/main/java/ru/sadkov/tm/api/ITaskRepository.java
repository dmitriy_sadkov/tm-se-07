package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {
    List<Task> findAll(String currentUser);

    void removeAll(String user);

    Task findOne(String taskId);

    void merge(Task task);

    void update(String taskId, String taskName);

    Task findTaskByName(String taskName, String userId);

    void removeByName(String taskName, String userId);
}
