package ru.sadkov.tm.api;

public interface IRepository<T> {
    void persist(T t);

    boolean isEmpty();
}
