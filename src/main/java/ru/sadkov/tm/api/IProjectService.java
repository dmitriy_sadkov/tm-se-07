package ru.sadkov.tm.api;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.ProjectRepository;

import java.util.List;

public interface IProjectService {
    String findProjectIdByName(String projectName, String userId);

    boolean saveProject(String projectName, String userId);

    void removeByName(String projectName, String userId);


    List<Project> showProjectsForUser(User user);

    void clearProjects(User currentUser);

    IProjectRepository getProjectRepository();


    void setProjectRepository(ProjectRepository projectRepository);

    void update(Project project, String projectName, String description);

    Project showByName(String projectName, User currentUser);
}
