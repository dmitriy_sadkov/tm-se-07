package ru.sadkov.tm.launcher;

import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.api.IUserService;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.service.ProjectService;
import ru.sadkov.tm.service.TaskService;
import ru.sadkov.tm.service.UserService;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    private final Scanner scanner = new Scanner(System.in);
    private IProjectService projectService = new ProjectService();
    private ITaskService taskService = new TaskService(projectService);
    private IUserService userService = new UserService();
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    public IUserService getUserService() {
        return userService;
    }

    public void setUserService(IUserService userService) {
        this.userService = userService;
    }

    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandMap.values());
    }

    public Scanner getScanner() {
        return scanner;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public void setProjectService(IProjectService projectService) {
        this.projectService = projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public void setTaskService(ITaskService taskService) {
        this.taskService = taskService;
    }

    private void registry(Class clazz) throws IllegalAccessException, InstantiationException {
        AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        final String commandName = abstractCommand.command();
        final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        abstractCommand.setServiceLocator(this);
        commandMap.put(commandName, abstractCommand);
    }

    private void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;

        if (abstractCommand.safe() || (!abstractCommand.safe() && userService.isAuth())) {
            abstractCommand.execute();
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    public void init(final Class... commandsClasses) {
        if (commandsClasses == null || commandsClasses.length == 0) return;
        try {
            for (Class clazz : commandsClasses) {
                registry(clazz);
            }
            userService.addTestUsers();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
