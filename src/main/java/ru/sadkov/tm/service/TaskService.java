package ru.sadkov.tm.service;

import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.TaskRepository;
import ru.sadkov.tm.util.RandomUtil;

import java.util.Iterator;
import java.util.List;

public final class TaskService implements ITaskService {
    private ITaskRepository taskRepository = new TaskRepository();
    private IProjectService projectService;

    public TaskService(IProjectService projectService) {
        this.projectService = projectService;

    }

    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task findTaskByName(final String taskName, final String userId) {
        if (taskName == null || taskName.isEmpty() || userId == null || userId.isEmpty()) return null;
        List<Task> tasks = taskRepository.findAll(userId);
        for (Task task : tasks) {
            if (task.getName().equals(taskName)) {
                return task;
            }
        }
        return null;
    }

    public void saveTask(final String taskName, final String projectName, final User currentUser) {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty() || currentUser == null)
            return;
        String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) {
            return;
        }
        taskRepository.merge(new Task(taskName, RandomUtil.UUID(), projectId, currentUser.getId()));
    }

    public void removeTask(final String taskName, final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        Task task = taskRepository.findTaskByName(taskName, currentUser.getId());
        if (task == null) return;
        taskRepository.removeByName(taskName, currentUser.getId());
    }

    public List<Task> findAll(final User user) {
        if (user == null) return null;
        if (taskRepository.findAll(user.getId()) == null || taskRepository.findAll(user.getId()).isEmpty()) {
            return null;
        }
        return taskRepository.findAll(user.getId());
    }


    public void clearTasks(final User user) {
        if (user == null) return;
        taskRepository.removeAll(user.getId());
    }

    public void removeTaskForProject(final String projectName, final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) {
            return;
        }
        final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        final Project project = projectService.getProjectRepository().findOne(projectId, currentUser.getId());
        if (project.getUserId().equals(currentUser.getId())) {
            Iterator<Task> iterator = taskRepository.findAll(currentUser.getId()).iterator();
            while (iterator.hasNext()) {
                Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskRepository.removeByName(task.getName(), currentUser.getId());
                }
            }
        }
    }

    public void update(Task task, final String taskName) {
        if (task == null || taskName == null || taskName.isEmpty()) return;
        taskRepository.update(task.getId(), taskName);
    }


}

