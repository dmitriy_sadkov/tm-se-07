package ru.sadkov.tm.service;

import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectService implements IProjectService {
    private IProjectRepository projectRepository = new ProjectRepository();

    public ProjectService() {
    }

    public String findProjectIdByName(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return null;
        List<Project> projects = new ArrayList<>(projectRepository.findAll(userId));
        for (Project project : projects) {
            if (project.getName().equalsIgnoreCase(projectName)) {
                return project.getId();
            }
        }
        return null;
    }

    public boolean saveProject(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return false;
        return projectRepository.merge(new Project(projectName, RandomUtil.UUID(), userId));
    }

    public void removeByName(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.remove(userId, projectName);
    }


    public List<Project> showProjectsForUser(final User user) {
        if (user == null) return null;
        return new ArrayList<>(projectRepository.findAll(user.getId()));
    }

    public void clearProjects(final User currentUser) {
        if (currentUser == null) return;
        projectRepository.removeAll(currentUser.getId());
    }

    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void update(Project project, final String projectName, final String description) {
        if (project == null || projectName == null || description == null || projectName.isEmpty() || description.isEmpty())
            return;
        projectRepository.update(project.getUserId(), project.getId(), projectName, description);
    }

    public Project showByName(final String projectName, final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOne(projectId, currentUser.getId());
    }
}
