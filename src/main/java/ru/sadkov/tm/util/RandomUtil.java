package ru.sadkov.tm.util;

import java.util.UUID;

public final class RandomUtil {

    public static String UUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

}
