package ru.sadkov.tm.repository;

import ru.sadkov.tm.api.IUserRepository;
import ru.sadkov.tm.entity.User;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public void persist(User user) {
        entities.put(user.getId(), user);
    }

    public User findOne(final String userId) {
        return entities.get(userId);
    }

    public User findByLogin(final String login) {
        List<User> userList = new ArrayList<>(entities.values());
        for (User user : userList) {
            if (user.getLogin().equals(login))
                return user;
        }
        return null;
    }

    public List<User> findAll() {
        List<User> userList = new ArrayList<>(entities.values());
        return userList;
    }

    public void removeByLogin(String login) {
        User toDelete = findByLogin(login);
        if (toDelete == null) return;
        entities.remove(toDelete.getId());
    }

    public void removeAll() {
        entities.clear();
    }

    public void update(final String userId, final String login) {
        entities.get(userId).setLogin(login);
    }

}
