package ru.sadkov.tm.repository;

import ru.sadkov.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ru.sadkov.tm.api.ITaskRepository {

    public List<Task> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        List<Task> result = new ArrayList<>();
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    public void persist(Task task) {
        entities.put(task.getId(), task);
    }

    public void removeByName(String taskName, String userId) {
        entities.values().removeIf(nextTask -> nextTask.getName().equalsIgnoreCase(taskName) &&
                nextTask.getUserId().equals(userId));
    }

    public void removeAll(final String userId) {
        entities.values().removeIf(nextTask -> nextTask.getUserId().equals(userId));
    }

    public Task findOne(final String taskId) {
        return entities.get(taskId);
    }

    public void merge(Task task) {
        if (entities.containsValue(task)) {
            update(task.getId(), task.getName());
        } else {
            persist(task);
        }
    }

    public void update(final String taskId, final String taskName) {
        Task task = findOne(taskId);
        task.setName(taskName);
    }

    public Task findTaskByName(final String taskName, final String userId) {
        for (Task task : findAll(userId)) {
            if (taskName.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }
}
