package ru.sadkov.tm.repository;

import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.entity.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public void persist(Project project) {
        entities.put(project.getId(), project);
    }

    public void remove(final String userId, final String projectName) {
        entities.values().removeIf(nextProject -> (nextProject.getName().equalsIgnoreCase(projectName)
                && (nextProject.getUserId().equals(userId))));
    }

    @Override
    public Collection<Project> findAll(String userId) {
        List<Project> result = new ArrayList<>();
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                result.add(project);
            }
        }
        return result;
    }

    public void removeAll(final String userId) {
        entities.values().removeIf(nextProject -> nextProject.getUserId().equals(userId));
    }

    public Project findOne(final String projectId, final String userId) {
        for (Project project : findAll(userId)) {
            if (project.getId().equals(projectId)) {
                return project;
            }
        }
        return null;
    }

    public boolean merge(Project project) {
        if (entities.containsValue(project)) {
            update(project.getUserId(), project.getId(), project.getDescription(), project.getName());
            return true;
        } else {
            persist(project);
            return true;
        }
    }

    public void update(final String userId, final String id, final String description, final String projectName) {
        Project project = findOne(id, userId);
        project.setName(projectName);
        project.setDescription(description);
    }
}
